let collection = [];

// Function to print the queue elements
function print() {
  return collection;
}

// Function to add an element to the queue
function enqueue(element) {
  collection.push(element);
  return collection;
}

// Function to remove an element from the queue
function dequeue() {
  if (collection.length === 0) { // this is just to check if the array is empty
    return "Queue is empty";
  }
  collection.shift();
  return collection;
}

// Function to get the first element of the queue
function front() {
  if (collection.length === 0) { // this is just to check if the array is empty
    return "Queue is empty";
  }
  return collection[0];
}

// Function to check if the queue is empty
function isEmpty() {
  return collection.length === 0;
}

// Function to get the size of the queue
function size() {
  return collection.length;
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};